import { Component, OnInit } from '@angular/core';
import { Comment } from './../../entities/comment';
import { CommentService } from './../../services/comment.service';

@Component({
  selector: 'app-component-listing',
  templateUrl: './component-listing.component.html',
  styleUrls: ['./component-listing.component.scss']
})
export class ComponentListingComponent implements OnInit {

  comments:Comment[];
  lastAuthorName: string;
  constructor(
    private commentService: CommentService
  ) {
    this.commentService.add(
      {
        id: 1,
        author: 'Bob',
        comment: 'I like pineapples',
        rating: 4 } as Comment
    );
    this.commentService.add(
      {
        id: 2,
        author: 'Gilroy',
        comment: 'I like soursop',
        rating: 3 } as Comment
    );


  }

  ngOnInit() {
    this.commentService.getAll().subscribe(allcomments=>this.comments = allcomments);
    this.commentService.getLastAuthorName().subscribe(author=>{
      this.lastAuthorName=(author)
    })
  }

}
